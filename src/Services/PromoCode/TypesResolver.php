<?php

namespace App\Services\PromoCode;

use App\Services\PromoCode\Exceptions\InvalidTypeException;
use App\Services\PromoCode\Types\FullType;
use App\Services\PromoCode\Types\NumericType;

class TypesResolver {

    /**
     * @param string $type
     * @return FullType|NumericType
     * @throws InvalidTypeException
     */
    public function resolve(string $type)
    {
        switch ($type){
            case "full":
                return new FullType();
            case "numeric":
                return new NumericType();
            default:
                throw new InvalidTypeException();
        }
    }

}