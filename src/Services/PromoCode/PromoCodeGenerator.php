<?php

namespace App\Services\PromoCode;

use App\Services\PromoCode\Generator\Generator;
use App\Services\PromoCode\Types\Type;

class PromoCodeGenerator {

    /** @var TypesResolver */
    private $typesResolver;

    /** @var Generator */
    private $generator;

    /**
     * Generate code based on integer, alphabet type and length
     *
     * @param Type $type
     * @param int $length
     * @param int $base
     * @return string
     * @throws Exceptions\InvalidTypeException
     */
    public function generate(string $type, int $length, int $base): string
    {
        $promoCodeType = $this->typesResolver->resolve($type);

        //Get last id for type of promocode
        $lastIdForType = 1;

        $this->generator->generate($promoCodeType, $length, $lastIdForType);
        //insert to database, if success

    }
}