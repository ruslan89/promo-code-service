<?php

namespace App\Services\PromoCode;

use Doctrine\ORM\EntityManagerInterface;

class PromoCodeActivator
{

    /** @var EntityManagerInterface */
    private $em;

    /**
     * PromoCodeActivator constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function activate(string $code): bool
    {
    }
}