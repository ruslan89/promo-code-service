<?php

namespace App\Services\PromoCode\Exceptions;

class InvalidTypeException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Invalid type exception');
    }
}