<?php

namespace App\Services\PromoCode\Types;

class NumericType implements Type
{

    public function getAlphabet(): string
    {
        return '0123456789';
    }
}