<?php

namespace App\Services\PromoCode\Types;

class FullType implements Type{

    public function getAlphabet(): string
    {
        return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }
}