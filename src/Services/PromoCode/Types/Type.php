<?php

namespace App\Services\PromoCode\Types;

interface Type
{
    public function getAlphabet(): string;
}