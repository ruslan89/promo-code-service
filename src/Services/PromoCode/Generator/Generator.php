<?php

namespace App\Services\PromoCode\Generator;

use App\Services\PromoCode\Types\Type;

class Generator
{

    /**
     * Generate code based on integer, alphabet type and length
     *
     * @param Type $type
     * @param int $length
     * @param int $base
     * @return string
     */
    public function generate(Type $type, int $length, int $base): string
    {
        $bitCount = floor(log(pow(strlen($type->getAlphabet()), $length), 2));
        $bitMask = (1 << $bitCount / 2) - 1;

        return $this->hash($base, $bitCount, $bitMask, $type->getAlphabet());
    }

    private function getBitCount(string $alphabet, $length)
    {
        return floor(log(pow(strlen($alphabet), $length), 2));
    }

    private function hash(int $number, int $bitCount, int $bitMask, string $alphabet)
    {
        $result = '';

        $left = $number >> ($bitCount / 2);
        $right = $number & $bitMask;
        for ($round = 0; $round < 10; ++$round) {
            $left = $left ^ (((($number ^ 47894) + 25) << 1) & $bitMask);
            $temp = $left;
            $left = $right;
            $right = $temp;
        }

        $cryptNumber = $left | ($right << ($bitCount / 2));

        for ($i = 0; $i < 6; ++$i) {
            $result .= $alphabet[(int)$cryptNumber & ((1 << 5) - 1)];
            $cryptNumber = $cryptNumber >> 5;
        }

        return $result;
    }
}